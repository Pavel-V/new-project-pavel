import React, { useState } from "react";
import Post from "./components/Post";
import "./styles/App.css";
import Comments from "./components/Comments";

function App() {
  const [posts, setPosts] = useState([
    {
      id: 1,
      title: "Название",
      description: "Описание",
      createdAt: "20 декабря",
      tag: "Создание сайтов",
      link: "https://www.google.ru/",
    },
    {
      id: 2,
      title: "Название",
      description: "Описание",
      createdAt: "21 декабря",
      tag: "Создание сайтов",
      link: "https://www.google.ru/",
    },

    {
      id: 1820,
      post_id: 1758,
      title: 'Anal Guneta',
      email: 'guneta_anal@schroeder.io',
      description: 'Reprehenderit similique qui. Pariatur consequuntur suscipit. Et recusandae rerum. Explicabo officia delectus.',
    }, {
      id: 1819,
      post_id: 1757,
      title: 'Menka Pothuvaal',
      email: 'menka_pothuvaal@will.co',
      description: 'Aperiam aliquam nisi. Est dolore occaecati. Sapiente officiis quis. Aut corrupti accusantium.',
    }, {
      id: 1811,
      post_id: 1749,
      title: 'Sarisha Bhattacharya',
      email: 'bhattacharya_sarisha@weber.org',
      description: 'Optio quibusdam porro. Tempore nihil sit.',
    },
  ]);


  const [title, setTitle] = useState("");

  const [description, setDescription] = useState("");

  const [tag, setTag] = useState("");

  const addPost = () => {
    const newPost = {
      id: posts.length + 1,
      title: title,
      description: description,
      createdAt: "21 декабря",
      tag: tag,
      link: "https://www.google.ru/",
    };

    setPosts([...posts, newPost]);
  };

  const removePost = (id) => {
    const newPosts = posts.filter((post) => {
      return post.id !== id;
    });
    setPosts(newPosts);
  };

  return (
    <div className="App">
      <div>
        {posts.map((post) => {
          return <Post key={post.id} {...post} removePost={removePost} />;
        })}
        <br></br>
        <input value={title} placeholder="Название" onChange={(e) => setTitle(e.target.value)}></input>
        <input
          placeholder="Описание"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        ></input>
        <input value={tag} placeholder="Тэг" onChange={(e) => setTag(e.target.value)}></input>
        <button onClick={addPost}>Добавить</button>
       
      </div>
        <br></br>
      <Comments />
    </div>
 
    
  );
}

export default App;
