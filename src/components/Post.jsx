import React from 'react';

const Post = (props) => {
    return (
        <div className="wrap">
        <div className="post">
          <div className="text">
      <h3>{props.title}</h3>
      <p className="description">{props.description}</p>
      <span className="info">
        <span className="x">
        <span className="date">{props.createdAt}</span>
        <span className="ellipse"></span>
        <span className="date">{props.tag}</span>
        <button onClick={() => props.removePost(props.id)}>Удалить</button>
        </span>
        <a href={props.link}>Перейти</a>
      </span>
    </div>
    </div>
    </div>

    
    );
};

export default Post;